# -*- coding: utf-8 -*-
import smtplib
import traceback
from datetime import datetime
from email.mime.text import MIMEText

from redis import StrictRedis

from settings import redis_config, EMAIL_ADDRESS, EMAIL_PASSWORD, RECIPIENTS
from web_modules import FlatDisposition, BezRealitky, UlovDomov


AVAILABLE_MODULES = {
    'bezrealitky.cz': BezRealitky,
    'ulovdomov.cz': UlovDomov
}


class FlatsCore(object):
    def __init__(self):
        self.rds = StrictRedis(**redis_config)

    def check_flats(self):
        results = {}
        for source, web_scraper in AVAILABLE_MODULES.items():
            try:
                ws = web_scraper()
                flats = ws.search(FlatDisposition.D3_1)
                results[source] = flats
            except Exception:
                print(traceback.format_exc())

        filtered_results = self.filter_results(results)
        print(filtered_results)
        if sum([len(i) for i in filtered_results.values()]) > 0:
            email = self.build_email_from_results(filtered_results)
            self.send_email(RECIPIENTS, email)

    def filter_results(self, flats):
        filtered_flats = {}
        for source, results in flats.items():
            filtered_flats[source] = []
            redis_key = 'flats-results:{0}'.format(source)
            for flat in results:
                if self.rds.sismember(redis_key, flat.flat_id):
                    continue
                filtered_flats[source].append(flat)
                self.rds.sadd(redis_key, flat.flat_id)
        return filtered_flats

    def build_email_from_results(self, results):
        msg = ''
        for source, flats in results.items():
            msg += '<h3><strong>{0}</strong></h3>\n'.format(source)
            for flat in flats:
                flat_msg = ''
                try:
                    flat_msg += ('<p style="padding-left: 20px;"><a title="{0}" href="{1}" target="_blank">{0}</a></p>\n'
                                 .format(flat.description.encode('utf-8'), flat.url))
                except UnicodeDecodeError:
                    flat_msg += ('<p style="padding-left: 20px;"><a title="{0}" href="{1}" target="_blank">{0}</a></p>\n'
                                 .format(flat.description, flat.url))
                flat_msg += '<ul style="list-style-type: disc;">\n'
                flat_msg += ('<li style="padding-left: 20px;">Pridané: {0}</li>\n'
                             .format(datetime.fromtimestamp(flat.timestamp).strftime('%Y-%m-%d %H:%M:%S')))
                try:
                    flat_msg += '<li style="padding-left: 20px;">Addresa: {0}</li>\n'.format(flat.address.encode('utf-8'))
                except UnicodeDecodeError:
                    flat_msg += '<li style="padding-left: 20px;">Addresa: {0}</li>\n'.format(flat.address)
                try:
                    flat_msg += '<li style="padding-left: 20px;">Cena: {0}</li>\n'.format(flat.price.encode('utf-8'))
                except AttributeError:
                    flat_msg += '<li style="padding-left: 20px;">Cena: {0}</li>\n'.format(flat.price)
                flat_msg += ('<li style="padding-left: 20px;">Rezervované: {0}</li>\n'
                             .format('ANO' if flat.reserved else 'NIE'))
                flat_msg += '</ul>\n'
                flat_msg += ('<p style="text-align: center;"><img src="{0}" alt="" width="300" height="225" /></p>\n'
                             .format(flat.image_url))
                flat_msg += '<p style="text-align: left;">&nbsp;</p>\n'
                msg += flat_msg
        print(msg)
        return MIMEText(msg, 'html')

    def send_email(self, recipients, email):
        server = smtplib.SMTP('mail.cernekm.com', 25)
        server.starttls()
        server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
        email['Subject'] = u'Nové byty v ponuke'
        email['From'] = EMAIL_ADDRESS
        email['To'] = ', '.join(recipients)
        server.sendmail(EMAIL_ADDRESS, recipients, email.as_string())
        server.quit()
