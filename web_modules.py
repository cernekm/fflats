# -*- coding: utf-8 -*-
from collections import namedtuple

import ujson
from grab import Grab
from datetime import datetime
from time import mktime


Flat = namedtuple('Flat', 'flat_id address description price image_url reserved url timestamp')


class FlatDisposition(object):
    D3_1 = '3-1'


class BaseWebModule(object):

    def __init__(self):
        pass


class BezRealitky(BaseWebModule):

    def search(self, dispositions):
        if not isinstance(dispositions, list):
            dispositions = [dispositions]

        g = Grab()
        g.go('https://www.bezrealitky.cz')

        headers = {
            'Content-Type': 'application/json'
        }

        payload = {
            'action': 'map',
            'filter': {
                'advertoffertype': 'nabidka-pronajem',
                'balcony': '',
                'construction': '',
                'description': '',
                'disposition': dispositions,
                'equipped': '',
                'estatetype': [
                    'byt'
                ],
                'order': 'time_order_desc',
                'ownership': '',
                'polygons': [[{
                  'lat': 49.211055455767,
                  'lng': 16.593672564460007
                }, {
                  'lat': 49.208485143475,
                  'lng': 16.595140459064964
                }, {
                  'lat': 49.210408036425,
                  'lng': 16.60291079577803
                }, {
                  'lat': 49.207690515351,
                  'lng': 16.604689342579036
                }, {
                  'lat': 49.209492205363,
                  'lng': 16.609645982116945
                }, {
                  'lat': 49.201452691343,
                  'lng': 16.615481089104037
                }, {
                  'lat': 49.202103561208,
                  'lng': 16.62197018880397
                }, {
                  'lat': 49.198688365198,
                  'lng': 16.622136820725018
                }, {
                  'lat': 49.199498702461,
                  'lng': 16.625962971604963
                }, {
                  'lat': 49.198175975799,
                  'lng': 16.62918869539999
                }, {
                  'lat': 49.188282924413,
                  'lng': 16.628868243803936
                }, {
                  'lat': 49.185328280001,
                  'lng': 16.618373704666055
                }, {
                  'lat': 49.183022460991,
                  'lng': 16.61940353313298
                }, {
                  'lat': 49.182997981471,
                  'lng': 16.617864138768937
                }, {
                  'lat': 49.177756564109,
                  'lng': 16.61436158505103
                }, {
                  'lat': 49.175869818983,
                  'lng': 16.611037239798975
                }, {
                  'lat': 49.177946035266,
                  'lng': 16.607938822298024
                }, {
                  'lat': 49.170365782593,
                  'lng': 16.607103269327013
                }, {
                  'lat': 49.168049373904,
                  'lng': 16.604856120527984
                }, {
                  'lat': 49.165120433812,
                  'lng': 16.594851800164975
                }, {
                  'lat': 49.165910135821,
                  'lng': 16.590766213339975
                }, {
                  'lat': 49.16984365317,
                  'lng': 16.591396672806013
                }, {
                  'lat': 49.170536348668,
                  'lng': 16.588193809890072
                }, {
                  'lat': 49.172965782857,
                  'lng': 16.58898637230004
                }, {
                  'lat': 49.174265635993,
                  'lng': 16.58524139265205
                }, {
                  'lat': 49.17685374444,
                  'lng': 16.584774064697058
                }, {
                  'lat': 49.177353522452,
                  'lng': 16.586641326465042
                }, {
                  'lat': 49.180149281042,
                  'lng': 16.584936545700998
                }, {
                  'lat': 49.177879509367,
                  'lng': 16.577961917514017
                }, {
                  'lat': 49.179270403308,
                  'lng': 16.57216846501001
                }, {
                  'lat': 49.18149780295,
                  'lng': 16.571045952839995
                }, {
                  'lat': 49.180906038964,
                  'lng': 16.565192534668995
                }, {
                  'lat': 49.184775610144,
                  'lng': 16.56661047609805
                }, {
                  'lat': 49.189273574557,
                  'lng': 16.56249032949904
                }, {
                  'lat': 49.193582728626,
                  'lng': 16.566998357405964
                }, {
                  'lat': 49.200865798411,
                  'lng': 16.567435208994993
                }, {
                  'lat': 49.200202988497,
                  'lng': 16.569220873343056
                }, {
                  'lat': 49.203481739315,
                  'lng': 16.57217382371698
                }, {
                  'lat': 49.201724791269,
                  'lng': 16.575052680311956
                }, {
                  'lat': 49.204303575631,
                  'lng': 16.579516543534055
                }, {
                  'lat': 49.2032955793,
                  'lng': 16.582611601033022
                }, {
                  'lat': 49.20547309721,
                  'lng': 16.58333650128702
                }, {
                  'lat': 49.211055455767,
                  'lng': 16.593672564460007
                }, {
                  'lat': 49.211055455767,
                  'lng': 16.593672564460007
                }, {
                  'lat': 49.211055455767,
                  'lng': 16.593672564460007
                }]],
                'priceFrom': None,
                'priceTo': None,
                'surfaceFrom': '',
                'surfaceTo': '',
                'terrace': ''
            },
            'squares': '[\"{\\\"swlat\\\":48,\\\"swlng\\\":16,\\\"nelat\\\":50,\\\"nelng\\\":20}\"]'
        }

        g.setup(post=ujson.dumps(payload), headers=headers)
        g.go('https://www.bezrealitky.cz/api/search/map')

        time_orders = {r['id']: r['time_order'] for square in g.doc.json['squares'] for r in square['records']}
        flat_ids = ['%22{0}%22'.format(r['id']) for square in g.doc.json['squares'] for r in square['records']]

        if not flat_ids:
            return None

        url = 'https://www.bezrealitky.cz/api/search/result?ids=%5B{0}%5D'.format(','.join(flat_ids))
        print(url)
        g.go(url)

        flats = []
        for record in g.doc.json['records']:
            flat = Flat(
                flat_id=record['id'],
                address=record['address'],
                description=record['short'],
                price=record['price'],
                image_url=record['mainImageUrl'],
                reserved=record['reserved'],
                url='https://www.bezrealitky.cz' + record['url'],
                timestamp=time_orders.get(record['id'])
            )
            flats.append(flat)

        return flats


class UlovDomov(BaseWebModule):

    def search(self, dispositions):
        dispositions = "7"

        g = Grab()
        g.go("http://www.ulovdomov.cz/")

        headers = {
            'Content-Type': 'application/json'
        }

        payload = {
              "query": " Brno-město",
              "offer_type_id": "1",
              "dispositions": dispositions,
              "price_from": "",
              "price_to": "",
              "acreage_from": "",
              "acreage_to": "",
              "added_before": "",
              "furnishing": [],
              "conveniences": [],
              "is_price_commision_free": True,
              "sort_by": "date:desc",
              "page": 1,
              "limit": 20,
              "bounds": {
                "north_east": {
                  "lng": 16.868068227051,
                  "lat": 49.402519363275
                },
                "south_west": {
                  "lng": 16.287852772949,
                  "lat": 49.000978442827
                }
              },
              "is_banner_premium_board": False,
              "is_banner_premium_board_brno": True,
              "test_2": None,
              "test_1": None
            }

        g.setup(post=ujson.dumps(payload), headers=headers)
        g.go("http://www.ulovdomov.cz/fe-api/find")

        flats = []
        for record in g.doc.json["offers"]:
            flat = Flat(
                flat_id=record['id'],
                address="{0} {1}".format(record['street']['label'].encode('utf-8'), record['street']['id']),
                description=record['description'],
                price=record['price_rental'],
                image_url=record['photos'][0]['path'],
                reserved=None,
                url=record['absolute_url'],
                timestamp=mktime(datetime.strptime(record['published_at'][:-5], "%Y-%m-%dT%H:%M:%S").timetuple())
            )
            flats.append(flat)

        return flats


if __name__ == '__main__':
    scraper = UlovDomov()
    scraper.search()
    # scraper = BezRealitky()
    # print(scraper.search(FlatDisposition.D3_1))
